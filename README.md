
# Labs de découverte d'Ansible.

---

La version en ligne de ce depot est disponible sur le site : [https://ebraux.gitlab.io/ansible-labs](https://ebraux.gitlab.io/ansible-labs)

---

- Accéder à la documentation des labs :  [labs](labs)
- Informations pour developpement du site : [development.md](development.md)

