# Utilisation en local du site

Build de l'image
```bash
docker build -t mkdocs_ansible-labs .
```

Lancement en mode "serveur"
```bash
docker run  -v ${PWD}:/work -p 8000:8000 mkdocs_ansible-labs mkdocs serve -a 0.0.0.0:8000 --verbose
```

Genération du site
```bash
docker run  -v ${PWD}:/work -p 8000:8000  mkdocs_ansible-labs mkdocs build --strict --verbose
```

Ménage
```bash
docker run -v ${PWD}:/work mkdocs_ansible-labs rm -rf /work/site
docker image rm mkdocs_ansible-labs
```

