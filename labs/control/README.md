

# Boucles : loop

exemple : installer plusieurs packages
```bash
ansible-playbook loop.yml
```

* [https://docs.ansible.com/ansible/2.9/user_guide/playbooks_loops.html](https://docs.ansible.com/ansible/2.9/user_guide/playbooks_loops.html)



## Conditional : When

exemple : Tester la Distribution d'un Managed-Node

On utilise une condition sur le "fact" "os_family" :
```bash
ansible-playbook when.yml
```


* [https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html](https://docs.ansible.com/ansible/latest/user_guide/playbooks_conditionals.html)
* [https://docs.ansible.com/ansible/2.9/user_guide/playbooks_conditionals.html#ansible-facts-os-family](https://docs.ansible.com/ansible/2.9/user_guide/playbooks_conditionals.html#ansible-facts-os-family)


## block 

exemple : détecter et gerer une exception
On utilise le module "debug" pour afficher des messages, et "command" pour gérer une erreur :
```bash
ansible-playbook block.yml
```

* [https://docs.ansible.com/ansible/2.9/user_guide/playbooks_blocks.html](https://docs.ansible.com/ansible/2.9/user_guide/playbooks_blocks.html)