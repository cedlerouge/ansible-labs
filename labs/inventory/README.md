# Gestion de l'inventory


## Utilisation de l'inventory

Pour afficher l'inventory comme il est vu par Ansible, utiliser la commande `ansible-inventory`


## Inventory basique

Le fichier 'inventory_basique' correspond à un inventory avec uniquement 2 Managed-Node.
```ini
srv-01
srv-02
```

Affichage de son contenu avec la commande `ansible-inventory` :

```bash
ansible-inventory -i inventory_basique --yaml --list
```

## Inventory complexe

Le fichier 'inventory_complexe' contient un inventory plus complexe, comprenant :

  - 2 groupes de serveurs, avec chacun 2 groupes enfants
  - 4 Managed-Nodes
  - certains Mananged-node appartiennent à plusieurs goupes

```ini
[prod:children]
frontend-prod
backend-prod

[rec:children]
frontend-rec
backend-rec

[frontend-prod]
srv-front-01
srv-front-02

[backend-prod]
srv-back-01

[frontend-rec]
srv-rec-01

[backend-rec]
srv-rec-01
```

Pour afficher son contenu avec la commande `ansible-inventory` :
```bash
ansible-inventory -i inventory_complexe --yaml --list
```


## Configuration d'Ansible dans l'inventory

Il est possible d'ajouter des informations de configuration spécifiques, ou des variables à chaque Managed-Node dans l'inventory : 

```bash
srv01 ansible_port=2201 ansible_host=localhost
srv02  ansible_port=2202 ansible_host=localhost
```

## Utilisation du formet YAML

L'inventory peut-être définit au format YAML

```yaml
all:
  children:
    prod:
      children:
        frontend-prod:
        backend-prod:

    rec:
      children:
        frontend-rec:
        backend-rec:

    frontend-prod:
      hosts:
        srv-front-01:
        srv-front-02:

    backend-prod:
      hosts:
        srv-back-01:

    frontend-rec:
      hosts:
        srv-rec-01:
          ansible_connection: ssh
          ansible_port: 22
          ansible_host: localhost
          ansible_user: root
          ansible_password: unsecure
          ansible_ssh_extra_args: '-o StrictHostKeyChecking=no'
    
```

Pour afficher son contenu avec la commande `ansible-inventory`
```bash
ansible-inventory -i inventory_complexe_yaml --yaml --list
```


## Informations complémentaires

* [https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html](https://docs.ansible.com/ansible/latest/user_guide/intro_inventory.html)