
# Utilisation de playbooks


##  Lancement d'un playbook qui ne fait rien

```yaml
- hosts: all
```
Mais qui nous apprend tout de même des choses ...

```bash
ansible-playbook  no-action.yml
```

```bash
PLAY [all] **************************

TASK [Gathering Facts] **************
ok: [srv-02]

PLAY RECAP **************************
srv-01  : ok=1 changed=0  unreachable=0  failed=0  skipped=0  rescued=0  ignored=0   
srv-02  : ok=1 changed=0  unreachable=0  failed=0  skipped=0  rescued=0  ignored=0   
```

On retrouve dans cette sortie :

* le filtre sur les Managed-Node à traiter, indiqué dans "hosts"  : `PLAY [all]`
* La liste des Managed-Nodes concernés "srv-01" et "srv-02"
* les actions menées, et leur résultat 

Même si aucune tache n'est présente dans le playbook, on obtient `ok=1`, qui indique qu'une tâche est effectuée. C'est une tâche "système" de collecte des données sur les Managed-Nodes (Gathering Facts), qui est effectuée par défaut. Pour la désactiver, il faut fixer la variable 'gather_facts' à 'no'.

```yaml
- hosts: all
  gather_facts: no
```
```bash
ansible-playbook  really-no-action.yml
```
```bash
PLAY [all] *************************
PLAY RECAP *************************
```

> La collecte des données sur les Managed-Node prend du temps et des ressources, et est inutile si on n'utilise pas ces données. La partie "Gathering Facts" est abordé dans le Lab sur les variables.

## Elévation des privilèges

Pour executer une tache en tant que root, il faut utiliser la variable "become". 

```bash
ansible-playbook  whoami.yml
```
```bash
    ...
    "whoamicommand.stdout_lines": [
        "user"
    ]
    ...
```

Par défaut l'utilisateur qui execute le playbook est celui utilisé la connexion.

Si on fixe `become: true`, on obtient

```bash
ansible-playbook  whoami-become.yml
```
```bash
    ...
    "whoamicommand.stdout_lines": [
        "root"
    ]
    ...
```

> La variable become peut être utilisée au niveau d'une tâche, ou de l'ensemble du playbook.

> Par défaut, "become" change l'utilisateur en "root", mais un autre utilisateur peut être spécifié en utilisant la variable "become_user"



## Lancement d'un playbook qui installe un package

Installation du package "fail2ban", en utilisant le module apt, ce qui donne la syntaxe YAML suivante :
```yaml
  - apt: 
    name: fail2ban
    state: latest
```

Lancement du playbook :
```bash
ansible-playbook  apt.yml
```

Fail2ban n'était pas installé sur les Managed-Nodes, on a donc en résultat des opérations effectuées : `ok=1 changed=1`

Si on relance cette commande, comme fail2ban est déjà présent, on obtient : `ok=1 changed=0`


## Nommer les choses 

Ansible permet d'ajouter des noms pour décrire les différentes opérations ce qui donne la syntaxe YAML suivante :
```yaml
  - name: Installation d'un package avec le module apt
    apt: 
    ...
```

Lancement du playbook :
```bash
ansible-playbook  name.yml
```

On voit apparaitre dans le résultat le nom du PLAY, ainsi que le nom de la tâche :
```bash
PLAY [Playbook de demo apt] 
*******************************
...

TASK [Installation d'un package avec le module apt]
***************************************************
...
``` 

## Utilisation de variables

Des variables peuvent être utiliser pour améliorer la lisibilité et la réutilisabilité des playbooks, ce qui donne la syntaxe YAML suivante :
```yaml
  # déclaration de la variable
  vars:
    package_name: fail2ban
  
  tasks:
      ...   
      # utilisation de la variable
      name: "{{ package_name }}"
    ...
```

Lancement du playbook :
```bash
ansible-playbook  vars.yml
```

Ici la variable est utilisée pour définir le nom du package à installer par le module "apt", mais aussi dans la description de la tâche :
```bash
TASK [Installation du package 'fail2ban' avec le module apt]
```

## Affichage / Debug

La commande `free` utilisée dans le lab sur le mode Ad-hoc permettait d'afficher l'espace disponible sur les Managed-Nodes : 
```bash
ansible all  -m shell -a 'free -h'
```

Si on utilise un playbook pour lancer cette commande, on n'obtient aucune information en sortie. Cce n'est pas le but d'un playbook.
```bash
ansible-playbook free.yml
```

Toutefois, pour afficher le résultat de la sortie standard d'une commande, il est possible d'utiliser le module "debug": 

```bash
ansible-playbook free-output.yml
```


## Vérification de la syntaxe

La syntaxe YAML du playbook peut être testée avant le lancement 
```bash
ansible-playbook --syntax-check error.yml
```
```bash
ansible-playbook --syntax-check error.yml
ERROR! Syntax Error while loading YAML.
  did not find expected '-' indicator

The error appears to be in '/labs/ansible-playbooks/error.yml': line 9, column 5, but may be elsewhere in the file depending on the exact syntax problem.

The offending line appears to be:
   - name: Installation du package {{ package_name }} avec le module apt
    apt:
    ^ here
```

Message assez habituel, il faut bien le dire, lié à un problème d'indentation :
```bash
diff vars.yml error.yml
8c8
<   - name: Installation du package {{ package_name }} avec le module apt
---
>    - name: Installation du package {{ package_name }} avec le module apt
```

## Simulation du lancement d'un playbook

Il est possible de simuler le lancement du playbook, aucun changement ne sera apporté :
```bash
ansible-playbook --check vars.yml
```

---

## Informations Complémentaires

Quelques liens :

* [https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html](https://docs.ansible.com/ansible/latest/reference_appendices/YAMLSyntax.html)
* [https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html](https://docs.ansible.com/ansible/latest/user_guide/playbooks_intro.html)
* [https://docs.ansible.com/ansible/2.9/modules/apt_module.html](https://docs.ansible.com/ansible/2.9/modules/apt_module.html)





