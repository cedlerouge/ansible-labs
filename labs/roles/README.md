# Utilisation de Rôles


## Configuration du Lab

Ansible gère les rôles dans le dossier défini par la variable `roles_path`, dont la valeur est par défaut `/etc/ansible/roles`.

Pour ce lab, cette variable est redéfinie pour stocker les rôles dans le dossier "./roles"

```bash
cat ansible.cfg
---
...
roles_path     = ./roles
...
```

## Création d'un rôle basique : installation de 'cowsay'

Tache à réaliser :

- Installation du package 'cowsay'
- pas de variable, ni de valeur par défaut, ni de template


Rappel : structure de fichier de référence d'un rôle :

```bash
├── README.md     : description du role
├── defaults      : variables par défaut du rôle
│   └── main.yml
├── files         : contient des fichiers à déployer
├── handlers      : actions déclenchées par une notification
│   └── main.yml
├── meta          : metadonnées et notamment les dépendances
│   └── main.yml
├── tasks         : liste des tâches à exécuter
│   └── main.yml
├── templates     : des templates au format Jinja2
|   └── template.j2
└── vars          : autres variables pour le rôle
    └── main.yml
```

Les ressources necessaires se limitent donc à 1 tache :

- fichier pour définir une tache
```
cowsay
   `-- tasks
       `-- main.yml
```
-  contenu du fichier : `cat roles/cowsay/tasks/main.yml`
```yaml
---
- name: install cowsay
  apt:
    name: cowsay
    state: latest
```

Pour utiliser ce rôle, il faut l'appeler dans un playbook : `cat playbooks/my-cowsay.yml`
```yaml
- hosts: all
  roles:
  - cowsay
```

Lancement du playbook 
```bash
ansible-playbook playbooks/my-cowsay.yml
```

Test
```bash
ssh root@srv-01 cowsay hello
```

## Installation de cowsay en utilisant ansible galaxy

Une recherche sur le portail Ansible Galaxy [https://galaxy.ansible.com/](https://galaxy.ansible.com/) renvoit plusieurs résultats, dont  [https://galaxy.ansible.com/jsecchiero/cowsay](https://galaxy.ansible.com/jsecchiero/cowsay).


Installation du rôle
```bash
ansible-galaxy install jsecchiero.cowsay
```

Vérification
```bash
ll roles
```


Et appel du rôle `cat playbooks/jsecchiero-cowsay.yml`
```yaml
- hosts: all
  roles:
  - jsecchiero.cowsay
```

Lancement
```bash
ansible-playbook playbooks/jsecchiero-cowsay.yml
```

Si on regardre le rôle de plus près

- l'arborescence "standard" a été mise en place
- on retrouve 
    - une tache dans le fichier `tasks/main.yml`
    - la déclaration de la  valeur par défaut de la chaine à afficher 'roar' à afficher dans `defaults/main.yml`
    - les informations sur le rôle dans `meta/main.yml`
    - un playbook de test dans `test`
    - et biensur un fichier `README.md`

On peut donc modifier le comportement de ce role en modifient la variable 'roar' :
```yaml
  vars:
  - roar: 'Hello'
```

Affichage du playbook : `cat playbooks/jsecchiero-cowsay-hello.yml`

Lancement
```bash
ansible-playbook playbooks/jsecchiero-cowsay-hello.yml
```

## Utilisation d'un fichier de "requirement.yml"

La commande `ansible-galaxy` peut également être utilisée pour gérer les dépendances et versions des rôles.

Définition de la liste de srôle à installer, et leur version dans un fichier `requirements.yml` : 
```yaml
---
- src: jsecchiero.cowsay
```

Installation
``` bash
ansible-galaxy install -r requirements.yml
```

## Initialisation du squelette d'un rôle

La commande `ansible-galaxy` permet de générer un "squelette" qui correspond au standard d'Ansible 

```bash
cd roles
ansible-galaxy init test-role

tree test-role
---
├── defaults
│   └── main.yml
├── files
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── README.md
├── tasks
│   └── main.yml
├── templates
├── tests
│   ├── inventory
│   └── test.yml
└── vars
    └── main.yml
```


## Etude d'un rôle un peu plus évolué

Installons un rôle pour fail2ban, installé précédement : [https://galaxy.ansible.com/oefenweb/fail2ban](https://galaxy.ansible.com/oefenweb/fail2ban)

```bash
ansible-galaxy install oefenweb.fail2ban
```

- Consulter le fichier README.md, pour la liste des Options
- parcourir l'arborescence
- ...



## Informations Complémentaires

Quelques liens :

- Le portail Ansible Galaxy : [https://galaxy.ansible.com/](https://galaxy.ansible.com/)
- Le point d'entrée vers la documentation : [https://galaxy.ansible.com/docs/](https://galaxy.ansible.com/docs/)
- Rechercher du contenu dans ansible-galaxy : [https://galaxy.ansible.com/docs/finding/search.html](https://galaxy.ansible.com/docs/finding/search.html)



