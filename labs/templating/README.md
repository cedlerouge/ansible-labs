# Utilisation de template


## Exemple de tempate

Utilisation d'un fichier de template pour créer un fichier "userlist.conf" à partir du contenu d'une liste (login,email), avec les contraintes :

- ne pas affichier l'utilisateur 'admin' : utilisation de la condition `if`
- afficher les emails en minuscule : utilisation du filtre `lower()`


Fichier de template : userlist.j2

```jinja2
{% for user in users %}
  {% if user.login != 'admin' %}
    login={{ user.login }}, email={{ user.email|lower() }}
  {% endif %}
{% endfor %}
```

> Dans un vrai fichier de template, on ne peut pas mettre les indentations, car ce sera des espaces dans le fichier de destination.

Utilisation du module 'template"
```yaml
- name: Template a file to /etc/userlist.conf
  template:
    src: templates/userlist.j2
    dest: /etc/userlist.conf
```

Affichage du playbook, avec la liste des utilisateurs
```bash
cat create-userlist.yml
```

Affichage du template 
```bash
cat templates/userlist.j2
```

Lancement de la création du fichier sur les Managed-Nodes
```bash
ansible-playbook create-userlist.yml
```

Vérification du résultat
```bash
ssh srv-01 cat /etc/userlist.conf
---
login=user1, email=user1@acme.com
login=User2, email=user2@acme.com
```

---

## Informations Complémentaires

Quelques liens :

- Le module template : [https://docs.ansible.com/ansible/2.9/modules/template_module.html](https://docs.ansible.com/ansible/2.9/modules/template_module.html)
- Jinja2 : [https://jinja.palletsprojects.com/en/2.11.x/templates/](https://jinja.palletsprojects.com/en/2.11.x/templates/)
- Les filtres de transformation de contenu : [https://jinja.palletsprojects.com/en/2.11.x/templates/#builtin-filters](https://jinja.palletsprojects.com/en/2.11.x/templates/#builtin-filters)
