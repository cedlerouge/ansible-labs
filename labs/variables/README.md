# Gestion des variables dans Ansible


## Variables de Playbook

Définie dans la section `vars` d'un playbook :

```yaml
  # déclaration de la variable
  vars:
    package_name: fail2ban
  
  tasks:
      ...   
      # utilisation de la variable
      name: "{{ package_name }}"
    ...
```

Lancement du playbook :
```bash
ansible-playbook  playbook-vars.yml
```



## Les Facts

Lors de la connexion à un Managed-Node, Ansible collecte des données sur l'environnement physique et lociciel.

Pour afficher les facts collectés, on peut utiliset le module 'setup'

```bash
ansible srv-01  -m setup
```

On peut utiliser des filtres pour limiter les informations retournées

Informations sur le type de Node, ici un container Docker :
```bash
ansible srv-01  -m setup  -a "filter=ansible_virtualization_type"
```
```bash
  "ansible_facts": {
      "ansible_virtualization_type": "docker",
      "discovered_interpreter_python": "/usr/bin/python3"
  },
```

Informations sur la distribution :
```bash
ansible srv-01  -m setup -a "filter=ansible_distribution*"
```
```bash
  "ansible_facts": {
      "ansible_distribution": "Ubuntu",
      "ansible_distribution_file_parsed": true,
      "ansible_distribution_file_path": "/etc/os-release",
      "ansible_distribution_file_variety": "Debian",
      "ansible_distribution_major_version": "20",
      "ansible_distribution_release": "focal",
      "ansible_distribution_version": "20.04",
      "discovered_interpreter_python": "/usr/bin/python3"
  },
```

Les facts peuvent être utilisé dans les playbooks, comme n'importe quelle autre variable. Il faut s'assurer d'avoir activé la variable `gather_facts: yes` (valeur par défaut)

```bash
ansible-playbook  display-fact.yml
```


## Variables d'inventory

Les variables peuvent être définie dans l'inventory :

- au niveau groupe, de façon globale ou spécifiques
- au niveau des managed_node

La gestion des variables d'inventory est faite via un ensemble de fichiers structurés :

```bash
  ├── group_vars      : variables de groupes
  │   └── all.yml     :   - concerne tous les Managed-Nodes
  │   └── <name>.yml  :   - pour un groupe spécifique
  └── host_vars       : variables de Managed-Node
      └── <name>.yml  :   - pour un Managed-Node spécifique
```

Pour afficher les variables définies au niveau de l'inventory, utiliser la commande `ansible-inventory`

Pour afficher l'ensemble des variables
```bash
ansible-inventory --yaml --list
```

Pour afficher les variables définies pour un Managed-Node
```bash
ansible-inventory --yaml --host srv-01
```

## Exemple avec un inventory simple

Le dossier 'inventory_simple' contient :

- un fichier d'inventory basique
- une arborescence déclarant
  - une variable globale : 'sample_group_var'
  - une variable spécifique à chaque Managed-Node : 'sample_dedicated_host_var'

Fichier d'inventory simple
```ini
srv-01
srv-02
```

La structure de variables corresponant :
```bash
group_vars
 `-- all.yml     :  group_var_commune
host_vars
 |-- srv-01.yml  :  host_var_commune
 `-- srv-02.yml  :  host_var_commune
```

Affichage des variables d'inventory :
```bash
ansible-inventory -i inventory_simple/hosts --yaml --list
```
* on retrouve la variable 'group_var_commune' avec ma même valeur sur tous les Managed-Nodes
* la variable 'host_var_commune' est spécifique à chaque Managed-Nodes


## Exemple avec un inventory plus complexe

Le dossier 'inventory_complexe' contient :

- un fichier d'inventory comprenant
  - 2 groupes de serveurs, avec chacun 2 groupes enfants
  - 4 Managed-Nodes
  - certains Mananged-node appartiennent à plusieurs goupes
- une arborescence déclarant
  - une variable commune à chaque de groupe : 'group_var_commune'
  - une variables spécifique à chaque groupe : 'group_var_commune_<NOM_DU_GROUPE>'
  - une variable commune à chaque Managed-Node : 'host_var_commune'

Fichier d'inventory complexe
```ini
[prod:children]
frontend-prod
backend-prod

[rec:children]
frontend-rec
backend-rec

[frontend-prod]
srv-front-01
srv-front-02

[backend-prod]
srv-back-01

[frontend-rec]
srv-rec-01

[backend-rec]
srv-rec-01
```

La structure de variables corresponant :
```bash
group_vars
 |-- all.yml
 |-- backend-prod.yml
 |-- backend-rec.yml
 |-- frontend-prod.yml
 |-- frontend-rec.yml
 |-- prod.yml
 `-- rec.yml
host_vars
 |-- srv-back-01.yml
 |-- srv-front-01.yml
 |-- srv-front-02.yml
 `-- srv-rec-01.yml
```


Affichage des variables d'inventory :
```bash
ansible-inventory -i inventory_complexe/hosts --yaml --list
```

* on retrouve la variable 'group_var_commune' avec la valeur du groupe enfant. La valeur du groupe parent est écrasée.
* on retrouve les variables 'group_var_specific<NOM_DU_GROUPE>' de tous les groupes (all, parent, child)
* la variable 'host_var_commune' est spécifique à chaque Managed-Nodes

Quelques mises en garde : 

* Pour le managed-Node qui était présent dans 2 groupes, la valeur "group_var_commune" une partie des données est donc perdue !!
* L'odre de priorité est important !!
* Attention a la cohérence entre les variables et les groupes de l'inventory



## Informations Complémentaires

Quelques liens :

* [https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html)
* [https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#understanding-variable-precedence](https://docs.ansible.com/ansible/latest/user_guide/playbooks_variables.html#understanding-variable-precedence)
* [https://docs.ansible.com/ansible/latest/user_guide/playbooks_vars_facts.html#vars-and-facts](https://docs.ansible.com/ansible/latest/user_guide/playbooks_vars_facts.html#vars-and-facts)


Pour aller plus loin :

* [https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html#defaulting-undefined-variables](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html#defaulting-undefined-variables)
* [https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html#defaulting-undefined-variables](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html#defaulting-undefined-variables)
* [https://docs.ansible.com/ansible/latest/reference_appendices/special_variables.html](https://docs.ansible.com/ansible/latest/reference_appendices/special_variables.html)
* [https://jinja.palletsprojects.com/en/2.11.x/templates/#builtin-filters](https://jinja.palletsprojects.com/en/2.11.x/templates/#builtin-filters)
