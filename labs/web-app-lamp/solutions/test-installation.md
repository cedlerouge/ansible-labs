## Utilisation du module `stat` et de `register` pour un traitement conditionnel
```yaml
- name: tester si le site est deja deploye
  stat:
    path: ...
  register: config

- name: Telecharge et decompresse l'archive de Wordpress
  unarchive:
    remote_src: yes
    src: ...
    dest: ...
    extra_opts:
      - '--strip-components=1'
  when: not config.stat.exists
```